# Spincoater

Source for the spincoater's speed control and user interface.

# Building

The original working build for this version of the spincoater was done using
[Arduino Create](create.arduino.cc), with the Adafruit_GFX and *modified*
Adafruit_SSD1306 library included in this package.
This should also be doable with the older Arduino IDE, or using AVRDude if
you're into that sort of thing. 
