// Adafruit_SSD1306-128-64 - Version: Latest
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#include <Wire.h>

/*
 * joystick x_pin   -> A0
 * joystick y_pin   -> A1
 * screen sda       -> A4 (SDA)
 * screen scl       -> A5 (SCL)
 * motor pin_a      -> 2
 * motor pin_b      -> 3
 * motor pin_fwd    -> 4
 * motor pin_bwd    -> 5
 * motor pin_pwm    -> 6
 * joystick button  -> 7
**/

/* MOTOR VARIABLES */
String mySt = "";
char myChar;
boolean stringComplete = false;  // whether the string is complete
boolean motor_start = false;
const byte pin_a = 2;     // for encoder pulse A
const byte pin_b = 3;     // for encoder pulse B
const byte pin_fwd = 4;   // for H-bridge: run motor forward
const byte pin_bwd = 5;   // for H-bridge: run motor backward
const byte pin_pwm = 6;   // for H-bridge: motor speed
int encoder = 0;
int m_direction = 0;
int sv_speed = 100;       // this value is 0~255
double pv_speed = 0;
double set_speed = 2000;
double e_speed = 0;       // error of speed = set_speed - pv_speed
double e_speed_pre = 0;   // last error of speed
double e_speed_sum = 0;   // sum error of speed
double pwm_pulse = 0;     // this value is 0~255
double pwm_pulse_pre = 30;
double kp = 0.003;
double ki = 0.00005;
double kd = 0.03;
int timer1_counter;       // for timer
long start_time = 0;
unsigned int runtime_millis = 15000;  // time for motor to run in millis, range 0-60s
int i=0;

/* JOYSTICK VARIABLES */
// Non changeable variables:
int joyInYPin = A0;                 // Y-Axis inputpin
int joyInXPin = A1;                 // X-Axis input pin
int joyInButton = 7;                // Button input pin
int debounceState = 0;              // Variable to store the debounce state
int isHeld = 0;                     // Variable to store the last debounce state
unsigned long lastDebounce = 0;     // Variable to store the last time the button was pressed
int valX = 0;                       // Variable to store the value of the X-axis in
int valY = 0;                       // Variable to store the value of the Y-axis in
int lastX = 0;                      // Variable to hold the last X value
long lastXMillis = 0;               // Variable to hold the last time X was pressed
int lastY = 0;                      // Varialble to hold the last Y value
long lastYMillis = 0;               // Variable to hold the last time Y was pressed
bool waited = true;                 // Has there been enought time between movement

// Changeable Variables, edit the values below to tweak operation.
unsigned long debounceTime = 100;   // Minimum time to wait between button state changes
int DEAD_ZONE = 150;                  // Joystick dead zone, gives a tiny null area in the middle of the stick to stop ghost values.
int nullX = 500;                    // average output of X axis at center
int nullY = 528;                    // average output of Y axis at center

// Debug settings
int debug_rawXY = 0;             // Should we output raw X/Y Data to serial? (1 = Show / 0 = hide)
int debug_rawButton = 0;         // Should we output raw button Data to serial? (1 = Show / 0 = hide)
int debug_processedButton = 1;   // Should we output processed button Data to serial? (1 = Show / 0 = hide)
int debug_processedJoystick = 1; // Should we output processed joystick data to serial? (1 = show / 0 = hide)

/* SCREEN VARIABLES */
// SCL GPIO5
// SDA GPIO4
#define OLED_RESET 0  // GPIO0
Adafruit_SSD1306 display(OLED_RESET);

#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif

/* MENU VARIABLES */
int screen = 0; // 0 -> MAIN; 1 -> SET_SPEED; 2 -> RUNNING
int cursor_position = 0;


void setup() {
  Serial.begin(9600);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.setRotation(2);
  display.display();
  delay(500);

  pinMode(pin_a,INPUT_PULLUP);
  pinMode(pin_b,INPUT_PULLUP);
  pinMode(pin_fwd,OUTPUT);
  pinMode(pin_bwd,OUTPUT);
  pinMode(pin_pwm,OUTPUT);
  pinMode(joyInButton,INPUT_PULLUP);


  attachInterrupt(digitalPinToInterrupt(pin_a), detect_a, RISING);
  // start serial port at 9600 bps:
  Serial.begin(9600);
  //--------------------------timer setup
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;
  timer1_counter = 59286;   // preload timer 65536-16MHz/256/2Hz (34286 for 0.5sec) (59286 for 0.1sec)


  TCNT1 = timer1_counter;   // preload timer
  TCCR1B |= (1 << CS12);    // 256 prescaler
  TIMSK1 |= (1 << TOIE1);   // enable timer overflow interrupt
  interrupts();             // enable all interrupts
  //--------------------------timer setup

  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  analogWrite(pin_pwm,0);   //stop motor
  digitalWrite(pin_fwd,0);  //stop motor
  digitalWrite(pin_bwd,0);  //stop motor


  display.clearDisplay();

}

void loop() {
  if (stringComplete) {
    // clear the string when COM receiving is completed
    mySt = "";  //note: in code below, mySt will not become blank, mySt is blank until '\n' is received
    stringComplete = false;
  }

  // Input from joystick
  lastX = valX;
  lastY = valY;
  valX = analogRead(joyInXPin);
  valY = analogRead(joyInYPin);

  //set_display({MENU, RUNNING, SET_SPEED for v2})
  //on detect_a run display();

  display_screen();

  // if (mySt.substring(0,8) == "vs_dir"){
  //   digitalWrite(pin_fwd,0);      //run motor run backward
  //   digitalWrite(pin_bwd,1);
  // }
  // set_display(SET_SPEED) v2
  if (mySt.substring(0,12) == "vs_set_speed"){
    set_speed = mySt.substring(12,mySt.length()).toFloat();  //get string after set_speed
  }
  if (mySt.substring(0,5) == "vs_kp"){
    kp = mySt.substring(5,mySt.length()).toFloat(); //get string after vs_kp
  }
  if (mySt.substring(0,5) == "vs_ki"){
    ki = mySt.substring(5,mySt.length()).toFloat(); //get string after vs_ki
  }
  if (mySt.substring(0,5) == "vs_kd"){
    kd = mySt.substring(5,mySt.length()).toFloat(); //get string after vs_kd
  }

  if (detect_up()) {
    Serial.println("UP");
  }
  if (detect_down()) {
    Serial.println("DOWN");
  }
  // if (detectClick()) {
  //   Serial.println("CLICK");
  // }
  // if (digitalRead(joyInButton) == HIGH) {
  //   Serial.println("HIGH");
  // }
}

/* display() {
  if (HOME) {
    set_speed
    start
  }
  if (RUNNING) {
    display set_speed,
  }
}
*/

void detect_a() {
  encoder+=1; //increasing encoder at new pulse
  m_direction = digitalRead(pin_b); //read direction of motor
}

ISR(TIMER1_OVF_vect)        // interrupt service routine - tick every 0.1sec
{
  TCNT1 = timer1_counter;   // set timer
  pv_speed = 60.0*(encoder/11.0)/0.1;  //calculate motor speed, unit is rpm
  encoder=0;
  //print out speed
  if (Serial.available() <= 0) {

    // Display in OLED
    // Serial.print("speed : ");
    // Serial.print(pv_speed);         //Print speed (rpm) value to Visual Studio
    // Serial.print("   Set Speed : ");
    // Serial.print(set_speed);         //Print set speed (rpm) value to Visual Studio
  }


  //PID program
  if (motor_start) {

    // display_running("running: " + String(pv_speed));

    e_speed = set_speed - pv_speed;
    pwm_pulse = pwm_pulse_pre + e_speed*kp + e_speed_sum*ki + (e_speed - e_speed_pre)*kd;
    pwm_pulse_pre = pwm_pulse;
    // Serial.print("   Error Speed : ");
    // Serial.print(e_speed);         //Print set speed (rpm) value to Visual Studio
    // Serial.print("   PWM : ");
    // Serial.println(pwm_pulse);         //Print set speed (rpm) value to Visual Studio
    e_speed_pre = e_speed;  //save last (previous) error
    e_speed_sum += e_speed; //sum of error
    if (e_speed_sum >4000) e_speed_sum = 4000;
    if (e_speed_sum <-4000) e_speed_sum = -4000;
    Serial.println(String(pwm_pulse));
    if (start_time + runtime_millis < millis()) {
      motor_start = false;
      digitalWrite(pin_fwd,0);
      digitalWrite(pin_bwd,0);
      display.clearDisplay();
      screen = 0;
    }
  }
  else{
    e_speed = 0;
    e_speed_pre = 0;
    e_speed_sum = 0;
    pwm_pulse = 0;
  }


  //update new speed
  if (pwm_pulse <255 & pwm_pulse >0){
    analogWrite(pin_pwm,pwm_pulse);  //set motor speed
  }
  else{
    if (pwm_pulse>255){
      analogWrite(pin_pwm,255);
    }
    else{
      analogWrite(pin_pwm,0);
    }
  }

}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    if (inChar != '\n') {
      mySt += inChar;
    }
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}

/**
 * SCREEN
 */



void display_screen() {
  if (screen == 0) {
    display_start_menu();
  }
  if (screen == 1) {
    display_speed_control();
  }
}

void display_start_menu() {
  if (detect_up()) {
    cursor_position = (cursor_position + 1) % 2;
  }
  if (detect_down()) {
    cursor_position = (cursor_position - 1) % 2;
  }

  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  if (motor_start) {
    display.println("RUNNING");
  }
  else {
    display.println(cursor_position == 0 ? "-> START" : "start");
    display.setCursor(0,10);
    display.println(cursor_position == 1 ? "-> SET SPEED" : "set speed");
  }
  display.setCursor(0,25);
  display.println("Speed: " + String(pv_speed));
  display.display();

  // if(signal == click) {
  if (detect_right()){
    if (cursor_position == 0) {
      start_time = millis();
      // Switch display mode to RUNNING
      // screen = 2;
      Serial.print("yes");
      digitalWrite(pin_fwd,1);      //run motor run forward
      digitalWrite(pin_bwd,0);
      motor_start = true;
    }
    else {
      display.clearDisplay();
      screen = 1;
    }
  }
}


void display_speed_control() {
  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Set speed to:");
  display.setCursor(20,10);
  display.print(String(set_speed) + "RPM");
  display.display();

  if (detect_down() && set_speed > 0) {
    set_speed -= 100;
  } else if (detect_up() && set_speed < 6000) {
    set_speed += 100;
  }
  if (detect_right()) {
    display.clearDisplay();
    screen = 0;
  }
}

// void display_running (String speed) {
//   display.setTextSize(1);
//   display.setTextColor(WHITE);
//   display.setCursor(0,0);
//   display.println(speed);
//   display.display();
// }

/**
 * JOYSTICK
 */

// boolean detectClick() {
//   //Button input
//   // if (debounceState == 1) {
//   //   if ((millis() -lastDebounce) >= debounceTime) {
//   //     if (digitalRead(joyInButton) == HIGH && isHeld == 0) {
//   //         //Button Released?
//   //         if (debug_rawButton == 1) {
//   //             Serial.println("Button Clicked with no hold.");
//   //         }
//   //         debounceState = 0;
//   //         Serial.println("Button Clicked");
//   //         return true;
//   //     }
//   //   }
//   // }
//   // if (debounceState == 0) {
//   //   if (digitalRead(joyInButton) == LOW) {
//   //       //Button pressed?
//   //       debounceState = 1;
//   //       lastDebounce = millis();
//   //   }
//   // }
//   if (debounceState == 1) {
//     if ((millis() - lastDebounce) >= debounceTime) {
//         if (digitalRead(joyInButton) == HIGH && isHeld == 0) {
//             Serial.println("3");
//             debounceState = 0;
//             return true;
//         }
//         if (digitalRead(joyInButton) == HIGH && isHeld == 1) {
//             debounceState = 0;
//             isHeld = 0;
//             // buttonReleased(millis() - lastDebounce);
//         }
//         if (digitalRead(joyInButton) == LOW) {
//             //Button still pressed??
//             isHeld = 1;
//             if (debug_rawButton == 1) {
//                 Serial.print("Button held for: ");
//                 Serial.println(millis() - lastDebounce);
//             }
//         }
//     }
//   }
//   if (debounceState == 0) {
//     if (digitalRead(joyInButton) == LOW) {
//         //Button pressed?
//         debounceState = 1;
//         lastDebounce = millis();
//     }
//   }
//   return false;
// }
detect_up() {
  waited = millis() - lastXMillis > 100;
  // lastXMillis = millis();
  // return valX > nullX && (valX - nullX) >= DEAD_ZONE && waited;

  if (valX > nullX && (valX - nullX) >= DEAD_ZONE && waited) {
    lastXMillis = millis();
    return true;
  }
  return false;
}
detect_down() {
  waited = millis() - lastXMillis > 100;
  if (valX < nullX && (nullX - valX) >= DEAD_ZONE && waited) {
    lastXMillis = millis();
    return true;
  }
  return false;
}
detect_left() {
  waited = millis() - lastYMillis > 100;
  if (valY < nullY && (nullY - valY) >= DEAD_ZONE && waited) {
    lastYMillis = millis();
    return true;
  }
  return false;

}
detect_right() {
  waited = millis() - lastYMillis > 100;
  if (valY > nullY && (valY - nullY) >= DEAD_ZONE && waited) {
    lastYMillis = millis();
    return true;
  }
  return false;
}

// void displayinit(){
//   // Display : ESP8266
//   display.init();
//   display.flipScreenVertically();
//   display.setFont(ArialMT_Plain_10);
//   display.setTextAlignment(TEXT_ALIGN_LEFT);
//   display.clear();
// }
